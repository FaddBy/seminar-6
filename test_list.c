#include "list.h"

int main() {
    struct vector* v = create_vector(3);
    vector_print(v, stdout);
    vector_add(v, 1);
    vector_print(v, stdout);
    vector_add(v, 2);
    vector_print(v, stdout);
    vector_add(v, 3);
    vector_print(v, stdout);
    vector_add(v, 4);
    vector_print(v, stdout);
    vector_change_count(v, 5);
    vector_print(v, stdout);
    vector_change_count_to_low(v, 3);
    vector_print(v, stdout);
    vector_free(v);
    return 0;
}