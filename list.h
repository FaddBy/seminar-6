#ifndef SEM6_LIST_H
#define SEM6_LIST_H
#include <inttypes.h>
#include <stdio.h>

struct vector;

struct vector* create_vector(size_t capacity);

int64_t* vector_int_at(struct vector* v, size_t i);

struct vector_info vector_get_info(struct vector* v);

void vector_add(struct vector* v, int64_t new);

void vector_add_all(struct vector* v, struct vector* to_add);

void vector_change_count(struct vector* v, size_t new_count);

void vector_change_count_to_low(struct vector* v, size_t new_count);

void vector_free(struct vector* v);

void vector_print(struct vector* v, FILE* f);

#endif
