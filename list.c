#include <inttypes.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include "list.h"

#define GROW_SCALE 2


struct vector {
    int64_t* array;
    size_t capacity;
    size_t count;
};

struct vector_info {
    size_t capacity;
    size_t count;
};

struct vector_info get_zero_vector_info() {
    return (struct vector_info) {.count = 0, .capacity = 0};
}

struct vector* create_vector(size_t capacity) {
    struct vector* v = malloc(sizeof(struct vector));
    v->array = malloc(capacity * sizeof(int64_t));
    v->capacity = capacity;
    v->count = 0;
}

int64_t* vector_int_at(struct vector* v, size_t i) {
    if (i >= v->count) { return NULL; }
    return v->array + i;
}

struct vector_info vector_get_info(struct vector* v) {
    if (!v) return get_zero_vector_info();
    return (struct vector_info) {.capacity = v->capacity, .count = v->count};
}

void vector_grow(struct vector* v) {
    if (!v) return;
    v->array = realloc(v->array, sizeof(int64_t) * v->capacity * GROW_SCALE);
    v->capacity *= GROW_SCALE;
}

void vector_add(struct vector* v, int64_t new) {
    if (!v) return;
    if (v->count == v->capacity) {
        vector_grow(v);
    }
    *(v->array + v->count) = new;
    v->count++;
}

void vector_add_all(struct vector* v, struct vector* to_add) {
    if (!v) return;
    for (size_t i = 0; i < to_add->count; i++) {
        vector_add(v, to_add->array[i]);
    }
}

void vector_change_count(struct vector* v, size_t new_count) {
    if (!v) return;
    while (new_count > v->capacity) {
        vector_grow(v);
    }
    v->count = new_count;
}

void vector_change_count_to_low(struct vector* v, size_t new_count) {
    if (new_count >= v->count)
        return;
    v->count = new_count;
}

void vector_free(struct vector* v) {
    v->count = 0;
    v->capacity = 0;
    free(v->array);
    free(v);
}

char* vector_info_repr(struct vector_info info, char* separator) {
    static const char* count_text = "count";
    static const char* capacity_text = "capacity";
    char* result = malloc(100);
    sprintf(result, "%s: %zu%s%s: %zu", count_text, info.count, separator, capacity_text, info.capacity);
    return result;
}

char* vector_element_repr(const int64_t* d, size_t i) {
    char* result = malloc(100);
    sprintf(result, "%zu: %" PRId64, i, *d);
    return result;
}

char* vector_array_repr(int64_t* v, size_t count, char* separator) {
    char* result = malloc(1000);
    if (count == 0) {
        sprintf(result, "");
        return result;
    }
    char* element = vector_element_repr(v, 0);

    sprintf(result, "%s%s", element, separator);
    free(element);
    for (size_t i = 1; i < count; i++) {
        element = vector_element_repr(v + i, i);
        char* element_text = malloc(100);
        sprintf(element_text, "%s%s", element, separator);
        free(element);
        strcat(result, element_text);
    }
    return result;
}

char* vector_repr(struct vector* v) {
    char* result = malloc(100);
    char* info = vector_info_repr(vector_get_info(v), "\n");
    char* array = vector_array_repr(v->array, v->count, "\n");
    sprintf(result, "%s\n%s", info, array);
    free(info);
    free(array);
    return result;
}

void vector_print(struct vector* v, FILE* f) {
    fprintf(f, "%s", vector_repr(v));
}


