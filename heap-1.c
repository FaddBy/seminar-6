/* heap-1.c */

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#define HEAP_BLOCKS 16
#define BLOCK_CAPACITY 1024

enum block_status {
    BLK_FREE = 0, BLK_ONE, BLK_FIRST, BLK_CONT, BLK_LAST
};

struct heap {
    struct block {
        char contents[BLOCK_CAPACITY];
    } blocks[HEAP_BLOCKS];
    enum block_status status[HEAP_BLOCKS];
} global_heap = {0};

struct block_id {
    size_t value;
    bool valid;
    struct heap *heap;
};

struct block_id block_id_new(size_t value, struct heap *from) {
    return (struct block_id) {.valid = true, .value = value, .heap = from};
}

struct block_id block_id_invalid() {
    return (struct block_id) {.valid = false};
}

bool block_id_is_valid(struct block_id bid) {
    return bid.valid && bid.value < HEAP_BLOCKS;
}

bool block_is_free(struct block_id bid) {
    return block_id_is_valid(bid) && bid.heap->status[bid.value] == BLK_FREE;
}

size_t get_block_count_by_size(size_t size) {
    return size / BLOCK_CAPACITY + (size % BLOCK_CAPACITY != 0);
}

struct block_id find_free_block_section(struct heap *heap, size_t block_count) {
    int free_blocks_in_a_row = 0;
    struct block_id first_free_block;
    for (size_t i = 0; i < HEAP_BLOCKS; i++) {
        first_free_block = block_id_new(i, heap);
        while (block_is_free(first_free_block)) {
            free_blocks_in_a_row++;
            if (free_blocks_in_a_row == block_count)
                return first_free_block;
        }
        free_blocks_in_a_row = 0;
    }
    return block_id_invalid();
}

void set_block_status(struct block_id bid, enum block_status new_status) {
    if (!block_id_is_valid(bid))
        return;
    bid.heap->status[bid.value] = new_status;
}

/* Allocate */
//? ? ?
struct block_id block_allocate(struct heap *heap, size_t size) {
    if (!heap)
        return block_id_invalid();
    size_t block_count = get_block_count_by_size(size);
    if (block_count > HEAP_BLOCKS || block_count <= 0)
        return block_id_invalid();
    struct block_id free_block_section = find_free_block_section(heap, block_count);
    if (!free_block_section.valid)
        return block_id_invalid();
    if (block_count == 1) {
        set_block_status(free_block_section, BLK_ONE);
        return free_block_section;
    }
    set_block_status(free_block_section, BLK_FIRST);
    struct block_id current_block = free_block_section;
    for (size_t i = 1; i < block_count - 1; i++) {
        current_block.value++;
        set_block_status(current_block, BLK_CONT);
    }
    current_block.value++;
    set_block_status(current_block, BLK_LAST);
    return free_block_section;
}

enum block_status get_block_status(struct block_id bid) {
    if (!block_id_is_valid(bid))
        return 0;
    return bid.heap->status[bid.value];
}
/* Free */
// ? ? ?
void block_free(struct block_id bid) {
    if (!block_id_is_valid(bid) || get_block_status(bid) != BLK_FIRST && get_block_status(bid) != BLK_ONE)
        return;
    if (get_block_status(bid) == BLK_ONE){
        set_block_status(bid, BLK_FREE);
        return;
    }
    do {
        set_block_status(bid, BLK_FREE);
        bid.value++;
    } while (block_id_is_valid(bid) && (get_block_status(bid) == BLK_CONT));
    if (block_id_is_valid(bid) && (get_block_status(bid) == BLK_LAST)) {
        set_block_status(bid, BLK_FREE);
    }
}

/* Printer */
const char *block_repr(struct block_id b) {
    static const char *const repr[] = {
            [BLK_FREE] = " .",
            [BLK_ONE] = " *",
            [BLK_FIRST] = "[=",
            [BLK_LAST] = "=]",
            [BLK_CONT] = " ="
    };
    if (b.valid)
        return repr[b.heap->status[b.value]];
    else
        return "INVALID";
}

void block_debug_info(struct block_id b, FILE *f) {
    fprintf(f, "%s", block_repr(b));
}

void block_foreach_printer(struct heap *h, size_t count,
                           void printer(struct block_id, FILE *f), FILE *f) {
    for (size_t c = 0; c < count; c++)
        printer(block_id_new(c, h), f);
}

void heap_debug_info(struct heap *h, FILE *f) {
    block_foreach_printer(h, HEAP_BLOCKS, block_debug_info, f);
    fprintf(f, "\n");
}

/*  -------- */

int main() {
    heap_debug_info(&global_heap, stdout);
    struct block_id bid1 = block_allocate(&global_heap, BLOCK_CAPACITY);
    heap_debug_info(&global_heap, stdout);
    struct block_id bid2 = block_allocate(&global_heap, 2 * BLOCK_CAPACITY);
    heap_debug_info(&global_heap, stdout);
    struct block_id bid3 = block_allocate(&global_heap, 2 * BLOCK_CAPACITY + 1);
    heap_debug_info(&global_heap, stdout);
    struct block_id bid4 = block_allocate(&global_heap, 4 * BLOCK_CAPACITY);
    heap_debug_info(&global_heap, stdout);
    block_free(bid2);
    heap_debug_info(&global_heap, stdout);
    block_free(bid3);
    heap_debug_info(&global_heap, stdout);
    block_free(bid1);
    heap_debug_info(&global_heap, stdout);
    block_free(bid4);
    heap_debug_info(&global_heap, stdout);


}
